package pl.silesiakursy;

public class Stars {
    public static void main(String[] args) {
        int n = 5;
        for (int i = 0; i < n; i++) {
            String row = "";
            for (int j = 0; j < n; j++) {
                if (j > i) {
                    row += j + 1;
                } else {
                    row += "*";
                }
            }
            System.out.print(row + "\n");
//                    * 2 3 4 5
//                    * * 3 4 5
//                    * * * 4 5
//                    * * * * 5
//                    * * * * *
        }
    }
}

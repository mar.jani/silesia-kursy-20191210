package pl.silesiakursy;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę");
        int numberOne = scanner.nextInt();
        System.out.println("Podaj drugą liczbę");
        int numberTwo = scanner.nextInt();

        if(numberOne > numberTwo){
            System.out.println("Większa liczba to: " + numberOne);
        }else if (numberOne < numberTwo){
            System.out.println("Większa liczba to: " + numberTwo);
        }else{
            System.out.println("Liczby są równe");
        }
    }
}

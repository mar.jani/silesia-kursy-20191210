package pl.silesiakursy;

public class StringInForAndWhile {
    public static void main(String[] args) {
        String string = "Lubię Java";

        for (int i = 0; i < 10 ; i++) {
            System.out.println(string);
        }

        int i = 0;
        while (i  < 10 ){
            System.out.println(string);
            i++;
        }

        for (int j = 0; j > -101 ; j--) {
            System.out.println(j);
        }

        int j = 0;
        while (j  > -101 ){
            System.out.println(j);
            j--;
        }
    }
}
